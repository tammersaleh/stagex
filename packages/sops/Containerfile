FROM scratch as base
ENV VERSION=3.8.1
ENV SRC_HASH=5ca70fb4f96797d09012c705a5bb935835896de7bcd063b98d498912b0e645a0
ENV SRC_FILE=${SRC_SITE}/v${VERSION}.tar.gz
ENV SRC_SITE=https://github.com/getsops/sops/archive/refs/tags/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=busybox . /
COPY --from=go . /
COPY --from=ca-certificates . /
RUN tar -xvf v${VERSION}.tar.gz
WORKDIR sops-${VERSION}
ENV PWD=/home/user/sops-${VERSION}
ENV GOPATH=${PWD}/cache/go
ENV GOCACHE=${PWD}/cache/
ENV GOWORK=off
ENV GOPROXY=https://proxy.golang.org,direct
ENV GOSUMDB=sum.golang.org
ENV CGO_ENABLED=0
ENV GOHOSTOS=linux
ENV GOHOSTARCH=amd64
ENV GOFLAGS=-trimpath
RUN mkdir -p ${GOPATH}
RUN go build -o bin/sops ./cmd/sops

from build as install
RUN <<-EOF
    mkdir -p /rootfs/usr/bin/
    cp bin/sops /rootfs/usr/bin/
EOF
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs/ /
