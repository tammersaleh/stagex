FROM scratch as base
ENV VERSION=2.6.4
ENV SRC_HASH=e87aae032bf07c26f85ac0ed3250998c37621d95f8bd748b31f15b33c45ee995
ENV SRC_FILE=flex-${VERSION}.tar.gz
ENV SRC_SITE=https://github.com/westes/flex/releases/download/v${VERSION}/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=busybox . /
COPY --from=perl . /
COPY --from=gcc . /
COPY --from=binutils . /
COPY --from=make . /
COPY --from=m4 . /
COPY --from=libtool . /
COPY --from=autoconf . /
COPY --from=automake . /
COPY --from=bison . /
COPY --from=gettext . /
COPY --from=musl . /
RUN tar -xzf ${SRC_FILE}
WORKDIR flex-${VERSION}
RUN --network=none <<-EOF
	set -eux
	./autogen.sh
	./configure \
		--build=x86_64-unknown-linux-musl \
		--host=x86_64-unknown-linux-musl \
		--prefix=/usr \
		--bindir=/bin \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info
	make LDFLAGS=-static
EOF

FROM build as install
RUN --network=none make DESTDIR=/rootfs install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
