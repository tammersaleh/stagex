FROM scratch as base
ARG ARCH=x86_64
ENV VERSION=2.35
ENV SRC_HASH=1b11659fb49e20e18db460d44485f09442c8c56d5df165de9461eb09c8302f85
ENV SRC_FILE=binutils-${VERSION}.tar.xz
ENV SRC_SITE=https://ftp.gnu.org/gnu/binutils/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stage3 . /
RUN tar -xf binutils-${VERSION}.tar.xz
WORKDIR binutils-${VERSION}
RUN --network=none <<-EOF
	set -ex
	CFLAGS='-Os -Wformat -Werror=format-security -O2' \
	CXXFLAGS='-Os -Wformat -Werror=format-security -O2' \
	./configure \
		--build=${ARCH}-linux-musl \
		--host=${ARCH}-linux-musl \
		--target=${ARCH}-linux-musl \
		--with-build-sysroot= \
		--with-sysroot=/ \
		--prefix=/usr \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--sysconfdir=/etc \
		--enable-plugins \
		--without-zstd \
		--disable-shared \
		--disable-jansson \
		--disable-gdb \
		--disable-gprofng \
		--disable-multilib \
		--disable-nls \
		--disable-werror \
		--enable-gold \
		--enable-64-bit-bfd \
		--enable-default-execstack=no \
		--enable-default-hash-style=gnu \
		--enable-deterministic-archives \
		--enable-ld=default \
		--enable-new-dtags \
		--enable-relro \
		--enable-threads \
		--with-mmap \
		--with-pic
	make -j "$(nproc)"
EOF

FROM build as install
RUN --network=none make DESTDIR="/rootfs" install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
