FROM scratch as base
ENV SRC_VERSION=1.6
ENV SRC_HASH=1393abd9adcf0762d34798dc34fdcf4d0d22a8410721e76f1e3afcd1daa4e2d1
ENV SRC_FILE=npth-${SRC_VERSION}.tar.bz2
ENV SRC_SITE=https://gnupg.org/ftp/gcrypt/npth/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} ${SRC_FILE}

FROM fetch as build
COPY --from=busybox . /
COPY --from=musl . /
COPY --from=gcc . /
COPY --from=binutils . /
COPY --from=make . /
RUN tar -xvf $SRC_FILE
WORKDIR npth-${SRC_VERSION}
RUN --network=none <<-EOF
	set -eux
	./configure \
		--build=x86_64-unknown-linux-musl \
		--host=x86_64-unknown-linux-musl \
		--prefix=/usr \
		--bindir=/bin \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
	make
EOF

FROM build as install
RUN make DESTDIR=/rootfs install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM base as test
COPY --from=install /rootfs /
COPY --from=zlib . /
COPY --from=musl . /
COPY --from=busybox . /
RUN /bin/sh <<-EOF
	set -eux
	EXPECTED_NPTH_VERSION="${SRC_VERSION}"
	NPTH_VERSION=\$(npth-config --version)
	if [ "\$NPTH_VERSION" != "\$EXPECTED_NPTH_VERSION" ]; then
	    echo "Expected npth version is \$EXPECTED_NPTH_VERSION, but got \$NPTH_VERSION"
	    exit 1
	fi
EOF

FROM scratch as package
COPY --from=install /rootfs /
