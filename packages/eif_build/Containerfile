FROM scratch as base
ENV VERSION=0.2.1
ENV SRC_HASH=da5421a705cdba20f9a01b38abfdfb0e31b6e4de50a61450bc9566b1205b25d1
ENV SRC_FILE=eif_build.tgz
ENV SRC_SITE=https://codeload.github.com/tkhq/eif_build/legacy.tar.gz/${VERSION}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} ${SRC_FILE}
COPY --from=busybox . /
COPY --from=musl . /
COPY --from=libunwind . /
COPY --from=zlib . /
COPY --from=openssl . /
COPY --from=ca-certificates . /
COPY --from=gcc . /
COPY --from=llvm . /
COPY --from=binutils . /
COPY --from=pkgconf . /
COPY --from=git . /
COPY --from=rust . /
# HACK: figure out why gcc package puts these in the wrong path at install time
COPY --from=gcc /usr/lib64/* /usr/lib/
RUN tar -xzf ${SRC_FILE}
RUN mv tkhq-eif_build-* eif_build
WORKDIR eif_build
RUN cargo fetch --locked

FROM fetch as build
RUN --network=none <<-EOF
    cargo build \
        --no-default-features \
        --locked \
        --release \
        --target x86_64-unknown-linux-musl
EOF

FROM build as install
WORKDIR /rootfs
RUN cp /eif_build/target/x86_64-unknown-linux-musl/release/eif_build .
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
