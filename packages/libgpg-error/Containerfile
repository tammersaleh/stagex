FROM scratch as base
ENV SRC_VERSION=1.47
ENV SRC_HASH=9e3c670966b96ecc746c28c2c419541e3bcb787d1a73930f5e5f5e1bcbbb9bdb
ENV SRC_FILE=libgpg-error-${SRC_VERSION}.tar.bz2
ENV SRC_SITE=https://gnupg.org/ftp/gcrypt/libgpg-error/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} ${SRC_FILE}

FROM fetch as build
COPY --from=busybox . /
COPY --from=musl . /
COPY --from=gcc . /
COPY --from=binutils . /
COPY --from=make . /
COPY --from=npth . /
RUN tar -xvf $SRC_FILE
WORKDIR libgpg-error-${SRC_VERSION}
RUN --network=none <<-EOF
	set -eux
	./configure \
		--build=x86_64-unknown-linux-musl \
		--host=x86_64-unknown-linux-musl \
		--prefix=/usr \
		--bindir=/bin \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
	make
EOF

FROM build as install
RUN make DESTDIR=/rootfs install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM base as test
COPY --from=install /rootfs /
COPY --from=busybox . /
RUN /bin/sh <<-EOF
	set -eux
	LIBGPG_ERROR_FILES_FOUND=\$(ls /usr/lib/ | grep libgpg-error || true)
	if [ -z "\$LIBGPG_ERROR_FILES_FOUND" ]; then
	    echo "libgpg-error not found"
	    exit 1
	fi
EOF

FROM scratch as package
COPY --from=install /rootfs /
