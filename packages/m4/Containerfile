FROM scratch as base
ENV VERSION=1.4.18
ENV SRC_HASH=ab2633921a5cd38e48797bf5521ad259bdc4b979078034a3b790d7fec5493fab
ENV SRC_FILE=m4-${VERSION}.tar.gz
ENV SRC_SITE=https://ftp.gnu.org/gnu/m4/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=busybox . /
COPY --from=gcc . /
COPY --from=binutils . /
COPY --from=make . /
COPY --from=musl . /
RUN tar -xzf ${SRC_FILE}
WORKDIR m4-${VERSION}
RUN --network=none <<-EOF
	set -eux; \
	./configure \
		--prefix=/usr; \
	make;
EOF

FROM build as install
RUN --network=none make DESTDIR=/rootfs install
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
