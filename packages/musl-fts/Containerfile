FROM scratch as base
ENV VERSION=1.2.7
ENV SRC_HASH=49ae567a96dbab22823d045ffebe0d6b14b9b799925e9ca9274d47d26ff482a6
ENV SRC_FILE=v${VERSION}.tar.gz
ENV SRC_SITE=https://github.com/void-linux/musl-fts/archive/refs/tags/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=busybox . /
COPY --from=gcc . /
COPY --from=binutils . /
COPY --from=make . /
COPY --from=musl . /
COPY --from=perl . /
COPY --from=autoconf . /
COPY --from=automake . /
COPY --from=libtool . /
COPY --from=pkgconf . /
COPY --from=m4 . /
RUN tar -xzf ${SRC_FILE}
WORKDIR musl-fts-${VERSION}
RUN --network=none <<-EOF
	set -eux
	./bootstrap.sh
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var
	make
EOF

FROM build as install
RUN <<-EOF
	set -eux
	make DESTDIR=/rootfs install
	install -Dm644 musl-fts.pc -t /rootfs/usr/lib/pkgconfig/
EOF
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
