FROM scratch as base
ENV VERSION=8.4.0
ENV SRC_HASH=16c62a9c4af0f703d28bda6d7bbf37ba47055ad3414d70dec63e2e6336f2a82d
ENV SRC_FILE=curl-${VERSION}.tar.xz
ENV SRC_SITE=https://curl.se/download/${SRC_FILE}

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=busybox . /
COPY --from=binutils . /
COPY --from=make . /
COPY --from=musl . /
COPY --from=gcc . /
COPY --from=openssl . /
RUN tar -xf curl-${VERSION}.tar.xz
WORKDIR curl-${VERSION}
RUN --network=none <<-EOF
    set -eux
    ./configure \
        --build=x86_64-linux-musl \
        --host=x86_64-linux-musl \
        --prefix=/usr \
        --sysconfdir=/etc \
        --mandir=/usr/share/man \
        --infodir=/usr/share/info \
        --with-ca-bundle=/etc/ssl/certs/ca-certificates.crt \
        --with-openssl \
        --enable-static-link
    make
EOF

FROM build as install
RUN make install DESTDIR=/rootfs
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
