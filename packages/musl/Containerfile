FROM scratch as base
ARG ARCH=x86_64
ENV VERSION=1.2.4
ENV SRC_FILE=musl-${VERSION}.tar.gz
ENV SRC_SITE=http://musl.libc.org/${SRC_FILE}
ENV SRC_HASH=7a35eae33d5372a7c0da1188de798726f68825513b7ae3ebe97aaaa52114f039
ENV CFLAGS="-Os -fstack-clash-protection -Wformat -Werror=format-security"
ENV CXXFLAGS="-Os -fstack-clash-protection -Wformat -Werror=format-security -D_GLIBCXX_ASSERTIONS=1 -D_LIBCPP_ENABLE_THREAD_SAFETY_ANNOTATIONS=1 -D_LIBCPP_ENABLE_HARDENED_MODE=1"
ENV LDFLAGS="-Wl,--as-needed,-O1,--sort-common -Wl,-soname,libc.musl-${ARCH}.so.1"

FROM base as fetch
ADD --checksum=sha256:${SRC_HASH} ${SRC_SITE} .

FROM fetch as build
COPY --from=stage3 . /
RUN tar -xzf ${SRC_FILE}
WORKDIR musl-${VERSION}
ADD *.patch .
RUN --network=none <<-EOF
	set -eux; \
	patch -p1 < lfs64.patch
	patch -p1 < lfs64-2.patch
	patch -p1 < relr-typedefs.patch
	./configure \
		--build=${ARCH}-linux-musl \
		--host=${ARCH}-linux-musl \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--infodir=/usr/share/info \
		--localstatedir=/var \
		--enable-debug
	make
EOF

FROM build as install
RUN --network=none <<-EOF
	set -eux
	make DESTDIR=/rootfs install
	mkdir -p /rootfs/usr/bin
	printf "%s\n%s\n" '#!/bin/sh' 'exec /lib/ld-musl-${ARCH}.so.1 --list "$@"' \
		> /rootfs/usr/bin/ldd; \
	chmod 755 /rootfs/usr/bin/ldd
	mv -f /rootfs/usr/lib/libc.so /rootfs/lib/ld-musl-${ARCH}.so.1
	ln -sf ld-musl-${ARCH}.so.1 /rootfs/lib/libc.musl-${ARCH}.so.1
	ln -sf ../../lib/ld-musl-${ARCH}.so.1 /rootfs/usr/lib/libc.so
EOF
RUN find /rootfs -exec touch -hcd "@0" "{}" +

FROM scratch as package
COPY --from=install /rootfs /
